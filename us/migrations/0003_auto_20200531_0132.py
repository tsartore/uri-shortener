# Generated by Django 3.0.3 on 2020-05-30 23:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('us', '0002_auto_20200522_2118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uri',
            name='base_uri',
            field=models.CharField(max_length=64),
        ),
        migrations.AlterField(
            model_name='uri',
            name='target_uri',
            field=models.CharField(default=None, max_length=200),
        ),
    ]
