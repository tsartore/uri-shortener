from django.db import models

# Create your models here.

class Uri(models.Model):
    base_uri = models.CharField(max_length=64)
    target_uri = models.CharField(max_length=200, default=None)
