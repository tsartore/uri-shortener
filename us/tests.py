from django.test import TestCase
from django.urls import reverse

from .models import Uri
from django.test.client import Client
import simplejson


# Create your tests here.

class TestModel(TestCase):

    def test_create_link(self):  # ok
        base_uri = "google"
        target_uri = "https://www.google.com/"
        uri = Uri(base_uri=base_uri, target_uri=target_uri)

        self.assertEqual(uri.base_uri, base_uri)
        self.assertEqual(uri.target_uri, target_uri)

    def test_create_unstring_link(self):  # ok
        base_uri = 123
        target_uri = "https://www.google.com/"
        uri = Uri(base_uri=base_uri, target_uri=target_uri)
        self.assertEqual(uri.base_uri, base_uri)
        self.assertEqual(uri.target_uri, target_uri)

    def test_create_strange_uri(self): # ok
        base_uri = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
        target_uri = "test"
        uri = Uri(base_uri=base_uri, target_uri=target_uri)
        self.assertEqual(uri.base_uri, base_uri)
        self.assertEqual(uri.target_uri, target_uri)


class ViewsTest(TestCase):

    def test_uri(self):  # ok
        json_uri = {"base_uri": "123", "target_uri": "https://www.google.fr/%22%7D"}
        json_data = simplejson.dumps(json_uri)
        response = self.client.post(reverse('uri_create'), json_data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

    def test_toolong_uri(self):
        json_uri = {"base_uri": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo.",
                    "target_uri": "https://www.google.fr/%22%7D"}
        json_data = simplejson.dumps(json_uri)
        response = self.client.post(reverse('uri_create'), json_data, content_type="application/json")
        self.assertNotEqual(response.status_code, 200)

    def test_toolong_url_uri(self):
        json_uri = {"base_uri": "123",
                    "target_uri": "https://www.google.com/search?client=firefox-b-d&q=Lorem+ipsum+dolor+sit+amet%2C+consectetur+adipiscing+elit.+Sed+vitae+dignissim+neque.+Sed+blandit+erat+ac+quam+pharetra+volutpat.+Sed+condimentum+quam+vel+ex+rhoncus+venenatis.+Aliquam+mollis+nisl+cras."}
        json_data = simplejson.dumps(json_uri)
        response = self.client.post(reverse('uri_create'), json_data, content_type="application/json")
        self.assertNotEqual(response.status_code, 200)

    def test_modif_uri(self):
        base_uri = "123"
        target_uri = "https://www.google.fr"
        uri = Uri.objects.create(base_uri=base_uri, target_uri=target_uri)
        new_target_uri = {"base_uri": "123", "target_uri": "https://www.yahoo.fr"}
        response = self.client.put(reverse('uri_management', args=[base_uri]), new_target_uri,
                                   content_type="application/json")
        self.assertEqual(response.status_code, 200)

    def test_modif_wrong_uri(self):
        base_uri = "123"
        target_uri = "https://www.google.fr"
        uri = Uri.objects.create(base_uri=base_uri, target_uri=target_uri)
        new_target_uri = {"base_uri": "12347", "target_uri": "https://www.yahoo.fr"}
        response = self.client.put(reverse('uri_management', args=[base_uri]), new_target_uri,
                                   content_type="application/json")
        self.assertEqual(response.status_code, 400)

    def test_delete_uri(self):
        base_uri = "123"
        target_uri = "https://www.google.fr"
        uri = Uri.objects.create(base_uri=base_uri, target_uri=target_uri)
        response = self.client.delete(reverse('uri_management', args=[base_uri]), content_type="application/json")
        self.assertEqual(response.status_code, 200)

    def test_delete_nonexistant_uri(self):
        base_uri = "123"
        base_uri_new = "1234"
        target_uri = "https://www.google.fr"
        uri = Uri.objects.create(base_uri=base_uri, target_uri=target_uri)
        response = self.client.delete(reverse('uri_management', args=[base_uri_new]), content_type="application/json")
        self.assertEqual(response.status_code, 404)
