from django.urls import path

from . import views

urlpatterns = [
    path('<base_uri>', views.uri_management, name='uri_management'),
]