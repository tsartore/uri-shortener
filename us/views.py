import json
import random

from django.core.exceptions import SuspiciousOperation
# Create your views here.
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from .models import Uri

base_uri_max_length = 64
target_uri_max_length = 200


def base_uri_generator():
    new_uri = ''.join([chr(random.randint(97, 122)) for i in range(10)])
    for i in range(100):
        if not Uri.objects.filter(base_uri=new_uri):
            return new_uri
    return False  # Return False if did not manage to generate URI


def uri_to_json(uri):
    uri_json = {'base_uri': uri.base_uri, 'target_uri': uri.target_uri}
    return uri_json


@csrf_exempt
def index(request):
    if request.method == "GET":
        return HttpResponse("Welcome to the uri_shortener API. Refer to the documentation to get started.")
    else:
        return HttpResponse(json.dumps({
            "error": "Method not allowed"}),
            content_type='application/json', status=405)


@csrf_exempt
def uri_create(request):
    if request.method != "POST":
        return HttpResponse(json.dumps({
            "error": "Method not allowed"}),
            content_type='application/json', status=405)

    request_json = json.load(request)
    if 'target_uri' not in request_json:  # Returns error if no target URI was specified
        return HttpResponse(json.dumps({
            "error": "Incomplete query: Missing target URI"}),
            content_type='application/json', status=400)
    if 'base_uri' not in request_json:  # If no base URI has been specified, generate one
        request_json['base_uri'] = base_uri_generator()
        if not request_json['base_uri']:
            return HttpResponse(json.dumps({
                "error": "Unable to generate base_uri. Please specify one"}),
                content_type='application/json', status=403)
    if len(request_json['base_uri']) > base_uri_max_length:
        return HttpResponse(json.dumps({
            "error": "base_uri is too long (max: " + str(base_uri_max_length) + " characters)"}),
            content_type='application/json', status=400)
    if len(request_json['target_uri']) > target_uri_max_length:
        return HttpResponse(json.dumps({
            "error": "target_uri is too long (max: " + str(target_uri_max_length) + " characters)"}),
            content_type='application/json', status=400)

    if Uri.objects.filter(base_uri=request_json['base_uri']):
        return HttpResponse(json.dumps({
            "error": "base_uri already taken"}),
            content_type='application/json', status=403)

    uri = Uri(base_uri=request_json['base_uri'], target_uri=request_json['target_uri'])
    uri.save()
    return JsonResponse(uri_to_json(uri))


@csrf_exempt
def uri_management(request, base_uri):
    if request.method != "POST":
        try:
            uri = Uri.objects.get(base_uri=base_uri)

            if request.method == "GET":
                return JsonResponse(uri_to_json(uri))
            elif request.method == "PUT":
                request_json = json.load(request)
                if not base_uri == request_json['base_uri']:
                    return HttpResponse(json.dumps({
                        "error": "You cannot change the base_uri parameter. Instead, create a new Uri object"}),
                        content_type='application/json', status=400)
                if len(request_json['target_uri']) > base_uri_max_length:
                    return HttpResponse(json.dumps({
                        "error": "target_uri is too long (max: " + str(target_uri_max_length) + " characters)"}),
                        content_type='application/json', status=400)
                uri.target_uri = request_json['target_uri']
                uri.save()
                return JsonResponse(uri_to_json(uri))
            elif request.method == "DELETE":
                uri.delete()
                return JsonResponse({"deleted": "true"})
        except Uri.DoesNotExist:
            return HttpResponse(json.dumps({
                "error": "This URI isn't registered. Please, check your spelling."}),
                content_type='application/json', status=404)

    else:
        return HttpResponse(json.dumps({
            "error": "Method not allowed"}),
            content_type='application/json', status=405)


@csrf_exempt
def uri_redirect(request, base_uri):
    try:
        uri = Uri.objects.get(base_uri=base_uri)
    except Uri.DoesNotExist:
        return HttpResponseNotFound("This URI isn't registered. Please, check your spelling.")

    try:
        return redirect(uri.target_uri)
    except SuspiciousOperation as DisallowedRedirect:
        return HttpResponse("For security reasons, we couldn't transfer you directly to your URI.<br>"
                            "So here is your URI instead: <a href=\"" + str(uri.target_uri) + "\">" + str(
            uri.target_uri) + "</a>",
                            status=200)
    except:
        uri.delete()
        return HttpResponse("Suspicious link detected. It has been removed", status=200)
